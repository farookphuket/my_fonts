# my_fonts

> my_fonts is the fonts that I use in my computer
> I am completely switch to `Arch Linux` in 10 Nov 2021 and from now on 
> I will use `Arch Linux` as it's really really fast.




### ============= how to use ================
> to setup is very simple all you have to do is clone this repo to your 
computer by run the command in your terminal `git clone https://gitlab.com/farookphuket/my_fonts.git` this will create the folder `my_fonts` in your `Home Directory` 
just copy the font you want to use to `/usr/share/fonts` or just run the below 
command :

`git clone https://gitlab.com/farookphuket/my_fonts.git && cd ~/my_fonts && sh copy_font.sh`

> then put your password hit enter all done!


### this font is a part of "archlinux_my_config"

> this font here is need to call by `archlinux_my_config` script when run the 
> installation to clone `archlinux_my_config` type `git clone https://github.com/farookphuket/archlinux_my_config.git` 
> in your terminal as the command here 

```
git clone https://github.com/farookphuket/archlinux_my_config.git 

cd ~/archlinux_my_config 

./setup.sh 


```


## my arch linux on 15 Nov 2021 

[use_arch_1]:https://i.ibb.co/VHBL2C2/use-arch-10-nov-2021-1.png
[use_arch_2]:https://i.ibb.co/tzc5SLP/use-arch-10-nov-2021-2.png
[use_arch_3]:https://i.ibb.co/V2tf2YF/use-arch-10-nov-2021-3.png


## Desktop
![my arch 1][use_arch_1]



## Terminal
![my arch 2][use_arch_2]


## Program Menu
![my arch 2][use_arch_3]


## last update 15 Nov 2021 
> fixed the problem of `cannot show icon in terminal` by install `not-fonts` 
> `noto-fonts-extra` 


## last update 26 May 2021 

> added nerd font [3270 Hack TH-FONTS]








---

## my config 26 May 2021

[my_config1]:https://i.ibb.co/m4bSyvL/2021-05-26-my-fonts.png

![my config 26 May 2021][my_config1]



