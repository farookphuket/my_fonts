#!/bin/bash




f_dir=~/my_fonts/FONTS
target_dir=/usr/share/fonts 


sudo cp -r $f_dir/3270 $target_dir/
sudo cp -r $f_dir/TH-FONT $target_dir/
sudo cp -r $f_dir/Hack $target_dir/
sudo cp -r $f_dir/Nerd_fonts $target_dir/


sudo fc-cache -vf 

sudo fc-match SauceCodePro NF -a  



# --- show the done message 

BACK_TITLE="\Z3 Operation has been done successfully"
TITLE="\Z2 Success! $USER please reboot your machine"
MSGBOX="\Z4 Success your fonts has been copy to directory as list below \
    \ndir 3270 -> /usr/share/fonts \
    \ndir Hack -> /usr/share/fonts \
    \ndir Nerd_fonts -> /usr/share/fonts \
    \ndir TH-FONT -> /usr/share/fonts \
    \n\Z1 if you're not see your new font in the fc-list \
    \n\Z1 please logout and login 
    \n\Z7 Thank you $USER for using the script 
    \n\Z6 Farook"

WIDTH=65
HEIGH=14

function goodbye(){
    dialog --clear \
        --colors \
        --backtitle "$BACK_TITLE" \
        --title "$TITLE" \
        --msgbox "$MSGBOX" \
        $HEIGH $WIDTH
}

goodbye
